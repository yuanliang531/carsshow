const express = require('express');
const app = express();

const request = require('request');
const bodyParser = require('body-parser');
const port = process.env.PORT || 4000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/cars', (req, res) => {
    request('http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars', (err, httpRes, body) => {
        console.log('error: ', err);
        console.log('status code: ', httpRes && httpRes.statusCode);
        // send data when succuss
        if(httpRes.statusCode == 200){
            let cars = constructData(JSON.parse(body));
            //console.log(cars);
            res.send(cars);
        }
        // return a code when can't find data
        if(httpRes.statusCode == 400){
            res.send('-1');
        }
    })
});

// construct data to make font end consume data conveniently 
const constructData = (data) => {
    let carsWithShow = [];
    for(let item of data){
        // get cars of each show
        let cars = item.cars;
        // store car with show name
        for(let car of cars){
            Object.assign(car, {"show": item.name, "id": Math.floor(Math.random()*1000)});
            carsWithShow.push(car);
        }
    }
    //console.log(carsWithShow);
    return carsWithShow;
}

app.listen(port, () => console.log(`Listening on port ${port}`));