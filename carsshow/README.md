## Install dependencies
type 'yarn' or 'yarn install' under root and client directory

## Run program
type 'yarn dev' under root directory

## Run server only
type 'node server.js' under root directory



