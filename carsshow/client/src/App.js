import React, { Component } from 'react';
import Item from './components/Item';

import './App.css';

class App extends Component {
  state = {
    cars: []
  }

  componentDidMount(){
    fetch('/cars')
      .then(res => res.json())
      .then(data => {
        console.log("data: ",data)
        this.setState({cars: data})
      });
  }

  render() {
    return (
      <div className="App">
        <Item cars={this.state.cars}/>
      </div>
    );
  }
}

export default App;
