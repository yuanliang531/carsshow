import React from 'react';
import '../App.css';

const Item = ({cars}) => {
    return <>
        <ul>
            {
                cars === -1 || cars.length === 0 ?
                <span>Cannot find data</span> :
                cars.sort((a, b) => {
                    if(a.make < b.make) return -1;
                    if(a.make > b.make) return 1;
                    return 0;
                }).map(car => (
                    <li key={car.id}>
                        <div className="make">{car.make}</div>
                        <div className="model">{car.model}</div>
                        <div className="show">{car.show}</div>
                    </li>
                ))
            }
        </ul>
    </>
}

export default Item;